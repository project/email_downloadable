<?php

/**
 * @file
 * Admin settings for the Email Downloadable module
 * @author Mateu Aguiló Bosch (mateu at humanbits dot es)
 */

/**
 * FAPI form for admin settings
 */
function email_downloadable_settings_form($form_state) {
  $options = node_get_types('names');
  $form['download_email_content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Downloadable content types'),
    '#description' => t('Select the content types you want users to download'),
    '#default_value' => variable_get('download_email_content_types', array()),
    '#options' => $options,
  );
  $form['placement'] = array(
    '#type' => 'fieldset',
    '#title' => t('Links placement'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['placement']['download_email_teaser'] = array(
    '#type' => 'checkbox',
    '#title' => t('Visible in teasers'),
    '#description' => t('Check this if you want the link appear in node teasers'),
    '#default_value' => variable_get('download_email_teaser', FALSE),
  );
  $form['placement']['download_email_link_placement'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Where do you want the send link?'),
    '#description' => t('Select the places you want the link to the form to appear.'),
    '#default_value' => variable_get('download_email_link_placement', array()),
    '#options' => array(
      'links' => t('Links section'),
      'content' => t('Content section'),
      'tab' => t('Tab in node view'),
    ),
  );
  $form['placement']['download_email_content_weight'] = array(
    '#type' => 'select',
    '#title' => t('Weight'),
    '#description' => t('Weight for the content. Only used in combination with %cs', array('%cs' => 'Content section')),
    '#default_value' => variable_get('download_email_content_weight', 0),
    '#options' => drupal_map_assoc(range(-99, 99)),
  );
  $form['download_email_salt'] = array(
    '#type' => 'textfield',
    '#title' => t('Salt'),
    '#description' => t('A text field with some text to randomize the codes. It does not matter what the content is, just change it.'),
    '#default_value' => variable_get('download_email_salt', 'yabadabadoo'),
  );
  $form['#submit'][] = 'email_downloadable_settings_form_submit';
  return system_settings_form($form);
}

function email_downloadable_settings_form_submit($form, &$form_state) {
  // Rebuild menu router
  variable_set('download_email_link_placement', $form_state['values']['download_email_link_placement']);
  drupal_set_message(t('Menu router has been rebuild'));
  menu_rebuild();
}