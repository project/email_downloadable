<?php

/**
 * @file
 * This file contains Rules integration code.
 */

/**
 * Implements hook_rules_event_info().
 */
function email_downloadable_rules_event_info() {
  return array(
    'download_requested' => array(
      'label' => t('User requested to download a link'),
      'module' => 'Email Downloadable',
      'arguments' => array(
        'account' => array('type' => 'user', 'label' => t('User requesting the node.')),
        'node' => array('type' => 'node', 'label' => t('Node being requested.')),
        'code' => array('type' => 'string', 'label' => t('Generated code.')),
      ),
    ),
    'download_visited' => array(
      'label' => t('User visited the downloadable node'),
      'module' => 'Email Downloadable',
      'arguments' => array(
        'node' => array('type' => 'node', 'label' => t('Node being downloaded.')),
        'code' => array('type' => 'string', 'label' => t('A valid code used to download the node.')),
      ),
    ),
  );
}