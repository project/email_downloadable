<?php

/**
 * @file
 * Module implementation file for the Email Downloadable file
 * @author Mateu Aguiló Bosch (mateu at humanbits dot es)
 */
 
/**
 * @todo
 *   TODO Authenticated users should be able to download without retyping email.
 */

define('EDWD_STATUS_PENDING', 1);
define('EDWD_STATUS_USED',    2);
define('EDWD_STATUS_EXPIRED', 3);

/**
 * Implements hook_menu().
 */
function email_downloadable_menu() {
  // Create the menu entries
  
  // Download page
  $items['node/%node/download/%'] = array(
    'title' => 'Download your coupon',
    'access callback' => 'email_downloadable_download_access',
    'access arguments' => array(1, 3),
    'page callback' => 'email_downloadable_download_page',
    'page arguments' => array(1, 3),
    'type' => MENU_CALLBACK,
  );
  
  // Send page form
  $items['node/%/send'] = array(
    'title' => 'Send to email',
    'access callback' => 'email_downloadable_send_access',
    'access arguments' => array('download content by email', 1),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('email_downloadable_send_form', 1),
    'type' => MENU_CALLBACK,
  );
  
  // Settings form
  $items['admin/settings/email-download'] = array(
    'title' => 'Email Downloadable',
    'description' => t('Adjust the settings for the Email Downloadable module'),
    'page arguments' => array('email_downloadable_settings_form'),
    'page callback' => 'drupal_get_form',
    'access arguments' => array('administer site configuration'),
    'file' => 'email_downloadable.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_menu_alter().
 */
function email_downloadable_menu_alter(&$items) {
  $places = variable_get('download_email_link_placement', array());
  if (isset($places['tab']) && $places['tab']) {
    $items['node/%/send']['type'] = MENU_LOCAL_TASK;
  }
}

/**
 * Implements hook_perm().
 */
function email_downloadable_perm() {
  return array('download content by email');
}

/**
 * Access callback for the email_downloadable_download_page
 * @param $verification_code
 *   The verification code to check out
 * @param $node
 *   The node to download
 * @return
 *   TRUE if the code and the node find a pending request status
 */
function email_downloadable_download_access($node, $code) {
  if (!$node) {
    return FALSE;
  }
  $query = "SELECT status FROM {email_downloadable} WHERE nid = %d AND code = '%s' AND status = %d";
  $st = db_result(db_query($query, $node->nid, $code, EDWD_STATUS_PENDING));
  // switch ($st) {
  //   case EDWD_STATUS_USED:
  //     drupal_set_message(t('The code you supplied has already been used. Request your code again.'), 'warning');
  //     break;
  // 
  //   case EDWD_STATUS_EXPIRED:
  //     drupal_set_message(t('The code you supplied has expired. Request your code again.'), 'warning');
  //     break;
  // }
  return $st == EDWD_STATUS_PENDING;
}

/**
 * Access callback for the email_downloadable_send_access
 * @param $perm
 *   The user_access perm
 * @param $nid
 *   A {node}.nid
 * @return
 *   TRUE if the user has access and the node exists
 */
function email_downloadable_send_access($perm, $nid) {
  if (!user_access($perm)) {
    return FALSE;
  }
  $query = "SELECT nid FROM {node} WHERE nid = %d";
  return db_result(db_query($query, $nid));
}

/**
 * Page callback redirecting to the actual node download
 */
function email_downloadable_download_page($node, $code) {
  $query = "SELECT status FROM {email_downloadable} WHERE code = '%s' AND nid = %d AND status = %d";
  $st = db_result(db_query($query, $code, $node->nid, EDWD_STATUS_USED));
  if ($st == EDWD_STATUS_USED) {
    $query = "UPDATE {email_downloadable} SET created = UNIX_TIMESTAMP() WHERE code = '%s' AND nid = %d AND status = %d";
    db_query($query, $code, $node->nid, EDWD_STATUS_USED);
    $query = "DELETE FROM {email_downloadable} WHERE code = '%s' AND nid = %d AND status = %d";
    db_query($query, $code, $node->nid, EDWD_STATUS_PENDING);
  }
  else {
    $query = "UPDATE {email_downloadable} SET status = %d WHERE code = '%s' AND nid = %d AND status = %d";
    db_query($query, EDWD_STATUS_USED, $code, $node->nid, EDWD_STATUS_PENDING);    
  }
  if (module_exists('rules')) {
    $params = array(
      'node' => $node,
      'code' => $code,
    );
    rules_invoke_event('download_visited', $params); 
  }
  return theme('edwd_node', $node);
}

/**
 * FAPI form to request an email to download the node
 */
function email_downloadable_send_form($form_state, $nid) {
  $form['nid'] = array(
    '#type' => 'hidden',
    '#value' => $nid,
  );
  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail'),
    '#description' => t('Type your email to get the link to download the content'),
    '#required' => TRUE,
  );
  $form['email']['#element_validate'][] = 'email_downloadable_email_validate';
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Validation function for the email element
 */
function email_downloadable_email_validate($element, &$form_state) {
  if (!valid_email_address($element['#value'])) {
    form_error($element, t('Please, use a valid email address.'));
  }
}

/**
 * Submit function for the email_downloadable_send_form
 */
function email_downloadable_send_form_submit($form, &$form_state) {
  _email_downloadable_insert_request($form_state['values']['nid'], $form_state['values']['email']);
  if (module_exists('rules')) {
    $account = (object)array(
      'uid' => 0,
      'mail' => $form_state['values']['email'],
      'name' => variable_get('anonymous', 'Anonymous'),
    );
    $params = array(
      'account' => $account,
      'node' => node_load($form_state['values']['nid']),
      'code' => md5($form_state['values']['email'] . '-' . variable_get('download_email_salt', 'yabadabadoo')),
    );
    rules_invoke_event('download_requested', $params); 
  }
  email_downloadable_send_code($form_state['values']['nid'], $form_state['values']['email']);
  drupal_set_message(t('A download link and instructions have been sent to %email', array('%email' => $form_state['values']['email'])));
  drupal_goto('node/' . $form_state['values']['nid']);
}

/**
 * Implements hook_link().
 */
function email_downloadable_link($type, $object, $teaser = FALSE) {
  $links = array();
  $places = variable_get('download_email_link_placement', array());
  if ($places['links']) {
    $types = variable_get('download_email_content_types', array());
    if (isset($object->nid) && is_numeric($object->nid) && $types[$object->type]) {
      if (!$teaser || variable_get('download_email_teaser', FALSE)) {
        $links['edwd_send'] = array(
          'title' => t('Send by email'),
          'href' => 'node/' . $object->nid . '/send',
          'attributes' => array(
            'title' => t('This will take you to a page where you will be asked for an email address to download the content.'),
            'class' => 'edwd-link edwd-send-content',
          ),
        );
      }
    }
  }

  return $links;
}

/**
 * Implements hook_nodeapi().
 */
function email_downloadable_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  $types = variable_get('download_email_content_types', array());
  if ($types[$node->type]) {
    $places = variable_get('download_email_link_placement', array());
    if ($places['content']) {    
      switch ($op) {
        case 'view':
          if ($a3 && !variable_get('download_email_teaser', FALSE)) {
            break;
          }
          $node->content['edwd_send'] = array(
            '#value' => l(t('Send by email'), 'node/' . $node->nid . '/send', array('attributes' => array(
              'title' => t('This will take you to a page where you will be asked for an email address to download the content.'),
              'class' => 'edwd-link edwd-send-content',
            ))),
            '#weight' => variable_get('download_email_content_weight', 0),
          );
          break;
      }
    }
  }
}

/**
 * Implements hook_theme().
 */
function email_downloadable_theme($existing, $type, $theme, $path) {
  return array(
    'edwd_node' => array(
      'arguments' => array('node' => NULL),
      // 'file' => 'email_downloadable.theme.inc',
      'template' => 'node',
      'preprocess functions' => array('template_preprocess_node', 'template_preprocess_edwd_node'),
      'path' => drupal_get_path('module', 'email_downloadable') . '/templates',
    ),
  );
}

function template_preprocess_edwd_node(&$variables) {
  $node = $variables['node'];
  $variables['template_files'] = array(
    'edwd-node-' . $node->nid,
    'edwd-node-' . $node->type,
    'edwd-node',
  );
}

/**
 * Helper function to insert a record to the DB with the information supplied
 * @param nid
 *   The node nid to download
 * @param email
 *   The validated email to autorize
 * @return
 *   void
 */
function _email_downloadable_insert_request($nid, $email, $status = EDWD_STATUS_PENDING) {
  $query = "SELECT nid FROM {email_downloadable} WHERE nid = %d AND code = MD5('%s-%s') AND status = %d";
  $res = db_result(db_query($query, $nid, $email, variable_get('download_email_salt', 'yabadabadoo'), $status));
  if ($res !== $nid) {
    $query = "INSERT INTO {email_downloadable} SET nid = %d, code = MD5('%s-%s'), created = UNIX_TIMESTAMP(), status = %d";
    db_query($query, $nid, $email, variable_get('download_email_salt', 'yabadabadoo'), $status);
  }
  else {
    $query = "UPDATE {email_downloadable} SET created = UNIX_TIMESTAMP() WHERE nid = %d AND code = MD5('%s-%s') AND status = %d";
    db_query($query, $nid, $email, variable_get('download_email_salt', 'yabadabadoo'), $status);
  }
}

/**
 * Send the email.
 */
function email_downloadable_send_code($nid, $email) {
  $code = md5($email . '-' . variable_get('download_email_salt', 'yabadabadoo'));
  drupal_mail('email_downloadable', 'edwd_code', $email, language_default(), array(
    'nid' => $nid,
    'code' => $code,
    'email' => $email,
  ));
}

/**
 * The function that actually sends the email
 */
function email_downloadable_mail($key, &$message, $params) {
  $language = $message['language'];
  global $base_url;
  $variables = array(
    '!site' => variable_get('site_name', 'Drupal'), 
    '!uri' => $base_url, 
    '!uri_brief' => preg_replace('!^https?://!', '', $base_url), 
    '!mailto' => $params['email'], 
    '!date' => format_date(time(), 'medium', '', NULL, $language->language), 
    '!login_uri' => url('user', array('absolute' => TRUE, 'language' => $language)), 
    '!edwd_url' => url('node/' . $params['nid'] . '/download/' . $params['code'], array('absolute' => TRUE)),
  );
  switch($key) {
    case 'edwd_code':
      $message['subject'] = t('Your download code from !site', $variables, $language->language);
      $message['body'][] = t("Dear user. Here is your download link. !edwd_url", $variables, $language->language);
      break;
  }
}
